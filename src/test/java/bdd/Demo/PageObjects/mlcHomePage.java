package bdd.Demo.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class mlcHomePage {
	public static WebDriver driver;
	private static WebElement element;
	
	public static WebElement mlcLogo(WebDriver driver){
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[1]/header[1]/div[1]/div[1]/a[1]/img[1]"));
		return element;
	}
	
	public static WebElement searchButton(WebDriver driver){
		element = driver.findElement(By.xpath("//NAV[@id=\"nav-onscreen\"]/BUTTON[@role=\"button\"][1]"));
		return element;
	}
	
	public static WebElement searchtxbx(WebDriver driver){
		element = driver.findElement(By.xpath("//INPUT[@id=\"q\"]"));
		return element;
	}
	
	public static WebElement lifeViewLink(WebDriver driver){
		element = driver.findElement(By.xpath("//strong[contains(.,'LifeView')]"));
		return element;
	}
	
}
