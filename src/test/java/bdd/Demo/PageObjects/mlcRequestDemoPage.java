package bdd.Demo.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class mlcRequestDemoPage {
	public static WebDriver driver;
	private static WebElement element;
	
	public static WebElement mlcLogo(WebDriver driver){
		element = driver.findElement(By.xpath("//DIV[1]/DIV[2]/HEADER[1]/DIV[1]/DIV[1]/A[1]/IMG[1]"));
		return element;
	}
	
	public static WebElement RequestDemoLabel(WebDriver driver){
		element = driver.findElement(By.xpath("//MAIN[@id=\"main\"]/HEADER[1]/H1[1]"));
												
		return element;
	}
	
	public static WebElement NameTxbx(WebDriver driver){
		element = driver.findElement(By.xpath("//MAIN[@id=\"main\"]/DIV[2]/FORM[1]/DIV[1]/INPUT[1]"));
		return element;
	}
	
	public static WebElement CompanyTxbx(WebDriver driver){
		element = driver.findElement(By.xpath("//MAIN[@id=\"main\"]/DIV[2]/FORM[1]/DIV[2]/INPUT[1]"));
		return element;
	}
	
	public static WebElement EmailTxbx(WebDriver driver){
		element = driver.findElement(By.xpath("//MAIN[@id=\"main\"]/DIV[2]/FORM[1]/DIV[3]/INPUT[1]"));
		return element;
	}
	
	public static WebElement PhoneTxbx(WebDriver driver){
		element = driver.findElement(By.xpath("//MAIN[@id=\"main\"]/DIV[2]/FORM[1]/DIV[4]/INPUT[1]"));
		return element;
	}
	
	public static WebElement ContactDateTxbx(WebDriver driver){
		element = driver.findElement(By.xpath("//MAIN[@id=\"main\"]/DIV[2]/FORM[1]/DIV[5]/INPUT[1]"));
		return element;
	}
	
	public static WebElement ContactTimeAM(WebDriver driver){
		element = driver.findElement(By.xpath("//TR/TD/LABEL[normalize-space()=\"AM\"]"));
		return element;
	}
	
	public static WebElement ContactTimePM(WebDriver driver){
		element = driver.findElement(By.xpath("//TR/TD/LABEL[normalize-space()=\"PM\"]"));
		return element;
	}
	
	public static WebElement RequestDetailTx(WebDriver driver){
		element = driver.findElement(By.xpath("//MAIN[@id=\"main\"]/DIV[2]/FORM[1]/DIV[7]/TEXTAREA[1]"));
		return element;
	}
}
