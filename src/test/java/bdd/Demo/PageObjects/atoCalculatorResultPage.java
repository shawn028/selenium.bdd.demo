package bdd.Demo.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class atoCalculatorResultPage {
	public static WebDriver driver;
	private static WebElement element;
	
	public static WebElement SimpleTaxCaculatorLogo(WebDriver driver){
		element = driver.findElement(By.xpath("//h1[contains(text(),'Simple tax calculator')]"));
		return element;
	}
	
	public static WebElement EstimateTaxResult(WebDriver driver){
		element = driver.findElement(By.xpath("//DIV[@id=\"applicationHost\"]/DIV[1]/DIV[1]/DIV[1]/DIV[2]/DIV[2]/DIV[1]/DIV[1]/DIV[1]/DIV[1]/DIV[1]/P[1]"));
		return element;
	}
	
	public static WebElement BackButton(WebDriver driver){
		element = driver.findElement(By.xpath("//button[contains(.,'Back')]"));
		return element;
	}
	
	public static WebElement Restartbutton(WebDriver driver){
		element = driver.findElement(By.xpath("//button[contains(.,'Restart')]"));
		return element;
	}
}
