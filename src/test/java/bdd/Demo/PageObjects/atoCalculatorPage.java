package bdd.Demo.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class atoCalculatorPage {
	public static WebDriver driver;
	private static WebElement element;
	
	public static WebElement SimpleTaxCaculatorLogo(WebDriver driver){
		element = driver.findElement(By.xpath("//h1[contains(text(),'Simple tax calculator')]"));
		return element;
	}
	
	public static Select IncomeYearDropdown(WebDriver driver){
		Select element = new Select(driver.findElement(By.name("ddl-financialYear")));
		return element;
	}
	
	public static WebElement TaxableIncomeTxbx(WebDriver driver){
		element = driver.findElement(By.name("texttaxIncomeAmt"));
		return element;
	}
	
	public static WebElement ResidentForFullYear(WebDriver driver){
		element = driver.findElement(By.cssSelector(".radio:nth-child(1) > .justify"));
		return element;
	}
	
	
	public static WebElement NoneResidentForFullYear(WebDriver driver){
		element = driver.findElement(By.xpath("//label[contains(.,'Non-resident for full year')]"));
		return element;
	}
	
	public static WebElement PartYearResident(WebDriver driver){
		element = driver.findElement(By.xpath("//label[contains(.,'Part-year resident')]"));
		return element;
	}
	
	public static Select ResidentPartYearMonths(WebDriver driver){
		Select element = new Select(driver.findElement(By.cssSelector("#ddl-residentPartYearMonths")));
		return element;
	}
	
	
	public static WebElement SubmitButton(WebDriver driver){
		element = driver.findElement(By.cssSelector("#bnav-n1-btn4"));
		return element;
	}
}
