package bdd.Demo.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class mlcLifeViewPage {
	public static WebDriver driver;
	private static WebElement element;
	
	public static WebElement mlcLogo(WebDriver driver){
		element = driver.findElement(By.xpath("//DIV[1]/DIV[1]/HEADER[1]/DIV[1]/DIV[1]/A[1]/IMG[1]"));
		return element;
	}
	
	public static WebElement lifeviewLable(WebDriver driver){
		element = driver.findElement(By.xpath("//MAIN[@id=\"main\"]/HEADER[1]/H1[1]"));
		return element;
	}
	
	public static WebElement HomeLink(WebDriver driver){
		element = driver.findElement(By.xpath("//LI/A[normalize-space()=\"Home\"]"));
		return element;
	}
	
	public static WebElement PartneringWithUsLink(WebDriver driver){
		element = driver.findElement(By.xpath("//DIV[1]/DIV[2]/DIV[3]/DIV[1]/DIV[1]/UL[1]/LI[2]/A[1]"));
		return element;
	}
	
	public static WebElement SuperannuationFunds(WebDriver driver){
		element = driver.findElement(By.xpath("//DIV[1]/DIV[2]/DIV[3]/DIV[1]/DIV[1]/UL[1]/LI[3]/A[1]"));
		return element;
	}
	
	public static WebElement LifeViewTx(WebDriver driver){
		element = driver.findElement(By.xpath("//DIV[1]/DIV[2]/DIV[3]/DIV[1]/DIV[1]/UL[1]/LI[4]"));
		return element;
	}
	
	public static WebElement RequestDemoButton(WebDriver driver){
		element = driver.findElement(By.xpath("//MAIN[@id=\"main\"]/DIV[2]/P[2]/A[1]"));
		return element;
	}
	
	
}
