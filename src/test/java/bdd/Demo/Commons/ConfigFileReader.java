package bdd.Demo.Commons;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
  private Properties properties;
	private final String propertyFilePath= ".\\resource\\env.properties";
	
	public ConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}

	public String getDriverPath(){
		String driverPath = properties.getProperty("driver.path.chrome");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");		
	}
	
	public long getImplicitlyWait() {		
		String implicitlyWait = properties.getProperty("implicitlyWait");
		if(implicitlyWait != null) return Long.parseLong(implicitlyWait);
		else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file.");		
	}
	
	public String getATOUrl() {
		String atourl = properties.getProperty("ato_url");
		if(atourl != null) return atourl;
		else throw new RuntimeException("url not specified in the Configuration.properties file.");
	}
	
	public String getMLCUrl() {
		String mlcurl = properties.getProperty("mlc_url");
		if(mlcurl != null) return mlcurl;
		else throw new RuntimeException("url not specified in the Configuration.properties file.");
	}
	
	public String getAPIBaseUrl() {
		String apibaseurl = properties.getProperty("api_base_url");
		if(apibaseurl != null) return apibaseurl;
		else throw new RuntimeException("url not specified in the Configuration.properties file.");
	}
	
	public String getAPIKey() {
		String apikey = properties.getProperty("api_key");
		if(apikey != null) return apikey;
		else throw new RuntimeException("apikey not specified in the Configuration.properties file.");
	}
}
