package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.asserts.SoftAssert;

import bdd.Demo.Commons.Log;

import bdd.Demo.Commons.ConfigFileReader;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.xml.DOMConfigurator;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import bdd.Demo.PageObjects.mlcHomePage;
import bdd.Demo.PageObjects.mlcLifeViewPage;
import bdd.Demo.PageObjects.mlcRequestDemoPage;

public class mlcSteps {
	WebDriver webdriver = new ChromeDriver();
	ConfigFileReader configFileReader;
	SoftAssert softAssert = new SoftAssert();
	
	@Given("^the user is navigate to the MLC home page$")
	public void TheUserNavigateToTheMLCHomePage() throws Throwable {
		DOMConfigurator.configure("log4j.xml");
		Log.startTestCase("MLC - Navigate pages then request DEMO");
		configFileReader = new ConfigFileReader();
		String AppUrl = configFileReader.getMLCUrl();
		long iWait = configFileReader.getImplicitlyWait();
		
		webdriver.get(AppUrl);
		webdriver.manage().window().maximize();// maximum the window
		webdriver.manage().timeouts().implicitlyWait(iWait, TimeUnit.SECONDS);

		String expectPageTitle = "Protecting you sits at the heart of everything we do | MLC Life Insurance";
		String actualPageTitle = webdriver.getTitle();
		
		softAssert.assertTrue(actualPageTitle.contains(expectPageTitle));
		
		Log.info("MLC - AppUrl is "+ AppUrl);
		Log.info("MLC - ImplicitlyWait is set to " + iWait);
		Log.info("MLC - TheUserNavigateToTheMLCHomePage - Completed");
	}
	
	@When("^the user search for 'Lifeview'$")
	public void TheUserSearchForLifeview() throws Throwable {
		mlcHomePage.searchButton(webdriver).click();
	    mlcHomePage.searchtxbx(webdriver).sendKeys("LifeView");
	    Thread.sleep(3000);
	    Log.info("MLC - TheUserSearchForLifeview - Completed");
	}
	
	@When("^click on 'Lifeview' link$")
	public void ClickOnLifeviewLink() throws Throwable {
		mlcHomePage.lifeViewLink(webdriver).click();
		Thread.sleep(3000);
		Log.info("MLC - ClickOnLifeviewLink - Completed");
	}
	
	@Then("^the user should see the 'LifeView' page with bread crumbs on the top 'Home','Partnering with us','Superannuation funds','LifeView'$")
	public void TheUserShouldSeeTheLifeViewPageWithBreadCrumbsOnTheTopHomePartneringWithUsSuperannuationFundsLifeView() throws Throwable {
		softAssert.assertEquals("Home",mlcLifeViewPage.HomeLink(webdriver).getText());
	    softAssert.assertEquals("Partnering with us",mlcLifeViewPage.PartneringWithUsLink(webdriver).getText());
	    softAssert.assertEquals("Superannuation funds",mlcLifeViewPage.SuperannuationFunds(webdriver).getText());
	    softAssert.assertEquals("LifeView",mlcLifeViewPage.LifeViewTx(webdriver).getText());
	    Thread.sleep(2000);
	    
	    Log.info("MLC - TheUserShouldSeeTheLifeViewPageWithBreadCrumbsOnTheTopHomePartneringWithUsSuperannuationFundsLifeView - Completed");
	}
	
	@Then("^click on 'Request a Demo' button$")
	public void ClickOnRequestADemoButton() throws Throwable {
		mlcLifeViewPage.RequestDemoButton(webdriver).click();
		Thread.sleep(2000);
		Log.info("MLC - ClickOnRequestADemoButton - Completed");
	}

	@Then("^Enter relevant data '\"([^\"]*)\"','\"([^\"]*)\"','\"([^\"]*)\"','(\\d+)','\"([^\"]*)\"','\"([^\"]*)\"','\"([^\"]*)\"'$")
	public void EnterRelevantData(String Name, String Company, String Email, int Phone, String PreferredContactDate, String PreferredContactTime, String RequestDetails) throws Throwable {
	    mlcRequestDemoPage.mlcLogo(webdriver).isDisplayed();
	    mlcRequestDemoPage.NameTxbx(webdriver).sendKeys(Name);
	    mlcRequestDemoPage.CompanyTxbx(webdriver).sendKeys(Company);
	    mlcRequestDemoPage.EmailTxbx(webdriver).sendKeys(Email);
	    mlcRequestDemoPage.PhoneTxbx(webdriver).sendKeys(String.valueOf(Phone));
	    mlcRequestDemoPage.ContactDateTxbx(webdriver).sendKeys(PreferredContactDate);
	    if (PreferredContactTime.matches("AM")) {
	    	mlcRequestDemoPage.ContactTimeAM(webdriver).click();
	    }
	    else if (PreferredContactTime.matches("PM")) {
	    	mlcRequestDemoPage.ContactTimePM(webdriver).click();
	    }
	    mlcRequestDemoPage.RequestDetailTx(webdriver).sendKeys(RequestDetails);
	    Thread.sleep(2000);
	    softAssert.assertAll();
	    
	    Log.info("MLC - Enter Name:" + Name);
	    Log.info("MLC - Enter Company:" + Company);
	    Log.info("MLC - Enter Email:" + Email);
	    Log.info("MLC - Enter Phone:" + Phone);
	    Log.info("MLC - Enter PreferredContactDate:" + PreferredContactDate);
	    Log.info("MLC - Enter PreferredContactTime:" + PreferredContactTime);
	    Log.info("MLC - Enter RequestDetails:" + RequestDetails);
	    Log.info("MLC - EnterRelevantData - Completed");
	    
	    webdriver.close();
	}
}
