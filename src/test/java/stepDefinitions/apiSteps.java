package stepDefinitions;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.xml.DOMConfigurator;
import org.testng.Assert;
import bdd.Demo.Commons.ConfigFileReader;
import bdd.Demo.Commons.Log;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;


import static io.restassured.RestAssured.given;

public class apiSteps {
	private ConfigFileReader configFileReader;
	public Response response;
	
	@When("^I request a shipping cost providing '\"([^\"]*)\"','\"([^\"]*)\"' and '(\\d+)'$")
	public void IRequestAShippingCostProvidingAnd(String CountryCode, String ServiceCode, int weight) throws Throwable {
		configFileReader = new ConfigFileReader();
		DOMConfigurator.configure("log4j.xml");
		Log.startTestCase("API - Request International parcel cost");
		
		String ApiBaseUrl = configFileReader.getAPIBaseUrl();
		String Key = configFileReader.getAPIKey();
		String RequestUrl = ApiBaseUrl + "country_code=" + CountryCode + "&weight=" + weight + "&service_code=" + ServiceCode;
		
		response = given()
				.header("AUTH-KEY",Key)
				.header("Content-Type","application/json")
				.get(RequestUrl);
		
		Log.info("API - CountryCode:" + CountryCode);
		Log.info("API - ServiceCode:" + ServiceCode);
		Log.info("API - weight:" + weight);
		Log.info("API - RequestUrl:" + RequestUrl);
		Log.info("API - IRequestAShippingCostProvidingAnd - Completed");
	}
	
	@Then("^I have received HTTP response code of (\\d+)$")
	public void IHaveReceivedHTTPResponseCodeOf(int expectedStatusCode) throws Throwable {
		int actualStatusCode = response.getStatusCode();
        assertEquals(actualStatusCode, expectedStatusCode);
        
        Log.info("API - actualStatusCode" + actualStatusCode);
        Log.info("API - expectedStatusCode" + expectedStatusCode);
        Log.info("API - expectedStatusCode - Completed");
	}
	
	@Then("^The response body should contain expected '\"([^\"]*)\"'$")
	public void TheResponseBodyShouldContainExpected(String expectedCost) throws Throwable {
		String responseBody = response.getBody().asString();
		Assert.assertTrue(responseBody.contains(expectedCost));
		
		Log.info("API - expectedCost" + expectedCost);
		Log.info("API - TheResponseBodyShouldContainExpected - Completed");
	}
	
}
