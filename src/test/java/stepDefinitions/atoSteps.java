package stepDefinitions;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.asserts.SoftAssert;

import bdd.Demo.Commons.Log;

import bdd.Demo.Commons.ConfigFileReader;

import org.apache.log4j.xml.DOMConfigurator;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import bdd.Demo.PageObjects.atoCalculatorPage;
import bdd.Demo.PageObjects.atoCalculatorResultPage;

public class atoSteps {
	WebDriver webdriver = new ChromeDriver();
	ConfigFileReader configFileReader;
	SoftAssert softAssert = new SoftAssert();
	
	@Given("^the user is navigate to the Simple Tax Caculator page$")
	public void TheUserNavigateToTheSimpleTaxCaculatorPage() throws Throwable {
		DOMConfigurator.configure("log4j.xml");
		Log.startTestCase("ATO - calculate page loads properly");
		configFileReader = new ConfigFileReader();
		String AppUrl = configFileReader.getATOUrl();
		long iWait = configFileReader.getImplicitlyWait();
		
		webdriver.get(AppUrl);
		webdriver.manage().window().maximize();// maximum the window
		webdriver.manage().timeouts().implicitlyWait(iWait, TimeUnit.SECONDS);

		String expectPageTitle = "Questions | Simple tax calculator";
		String actualPageTitle = webdriver.getTitle();
		
		softAssert.assertTrue(actualPageTitle.contains(expectPageTitle));
		
		Log.info("ATO - AppUrl is "+ AppUrl);
		Log.info("ImplicitlyWait is set to " + iWait);
		Log.info("ATO - TheUserNavigateToTheSimpleTaxCaculatorPage - Completed");
	}

	@When("^the user select (\\d+)-(\\d+)$")
	public void TheUserSelectIncomeYear(int Yr1, int Yr2) throws Throwable {
		String IncomeYear= Yr1 + "-" + Yr2;
		atoCalculatorPage.IncomeYearDropdown(webdriver).selectByVisibleText(IncomeYear);
		
		Log.info("ATO - IncomeYear is "+ IncomeYear);
		Log.info("ATO - TheUserSelectIncomeYear - Completed");
	}

	@When("^input(\\d+)$")
	public void InputIncome(int Income) throws Throwable {
		atoCalculatorPage.TaxableIncomeTxbx(webdriver).sendKeys(String.valueOf(Income));
		
		Log.info("ATO - Income is "+ Income);
		Log.info("ATO - InputIncome - Completed");
	}
	
	@When("^select ResidencyStatus \"([^\"]*)\"$")
	public void SelectResidencyStatus(String ResidencyStatus) throws Throwable {
		if (ResidencyStatus.matches("full")) {
			atoCalculatorPage.ResidentForFullYear(webdriver).click();
		}
		else if (ResidencyStatus.matches("none")) {
			atoCalculatorPage.NoneResidentForFullYear(webdriver).click();
		}
		else if (ResidencyStatus.matches("part")) {
			atoCalculatorPage.PartYearResident(webdriver).click();
			atoCalculatorPage.ResidentPartYearMonths(webdriver).selectByVisibleText("2");
		}
		
		Log.info("ATO - ResidencyStatus is "+ ResidencyStatus);
		Log.info("ATO - SelectResidencyStatus - Completed");
	}
	
	@Then("^Clciks on submit button$")
	public void ClciksOnSubmitButton() throws Throwable {
		Actions actions = new Actions(webdriver);
		actions.sendKeys(Keys.PAGE_DOWN).perform();
		Thread.sleep(1000);
		atoCalculatorPage.SubmitButton(webdriver).click();
		
		Log.info("ATO - ClciksOnSubmitButton - Completed");
	}

	@Then("^the user should see the \"([^\"]*)\"$")
	public void TheUserShouldSeeThe(String expectTax) throws Throwable {
		
		String displayTax = atoCalculatorResultPage.EstimateTaxResult(webdriver).getText();
		String estTax = "The estimated tax on your taxable income is "+ expectTax;
		softAssert.assertEquals(estTax,displayTax);
		softAssert.assertAll();
		
		Log.info("ATO - expectTax is "+ expectTax);
		Log.info("ATO - TheUserShouldSeeThe - Completed");
		
		webdriver.close();
	}

}
