## Selenium BDD DEMO 

This DEMO in Java language using Java libraries; RestAssured (for API Service) and Cucumber (for BDD styled), along with Maven as a build tool.

---

## Overview 

. This test framework uses Java as a main language with 'RestAssured' and 'Cucumber' libraries 

. Test will run according to feature files - which are stored under ./features folder. 

. Outputs (Json, XML and html files) will be produced under ./target/cucumber-reports folder.

. Logs (err,info and warn files) will be produced under ./logs folder.

---

## How to setup

. Maven is required to setup and run (https://maven.apache.org/install.html)

---

## How to run

. ATO Test Suite : "mvn test -Dcucumber.options="--tags @ATO"" to build and execute ato tests

. MLC Test Suite : "mvn test -Dcucumber.options="--tags @MLC"" to build and execute mlc tests

. API Test Suite : "mvn test -Dcucumber.options="--tags @API"" to build and execute api tests

. ALL Test Suite : "mvn test" to build and execute all tests

Once finished, there will be reports in ./target/cucumber-reports/* folder, and also, there will be logs in ./logs folder

---

## Where to find reports

. JSON file: ./target/cucumber-reports/Cucumber.json

. XML file: ./target/cucumber-reports/Cucumber.xml

. HTML file: ./target/cucumber-reports/html/index.html

---

## Example of execution,report and log

![testExecution.jpg](https://bitbucket.org/shawn028/selenium.bdd.demo/raw/fe04d613dd92/screenshots/testExecution.jpg?at=master)

![htmlReport.jpg](https://bitbucket.org/shawn028/selenium.bdd.demo/raw/fe04d613dd92/screenshots/htmlReport.jpg?at=master)

![infoLog.jpg](https://bitbucket.org/shawn028/selenium.bdd.demo/raw/fe04d613dd92/screenshots/infoLog.jpg?at=master)