Feature: MLC pages check then request Demo
  	
	@Sanity @MLC
  Scenario Outline: Navigate on MLC website then request a Demo 
  	Given the user is navigate to the MLC home page 
		When the user search for 'Lifeview' 
		And  click on 'Lifeview' link
		Then the user should see the 'LifeView' page with bread crumbs on the top 'Home','Partnering with us','Superannuation funds','LifeView'
	  And  click on 'Request a Demo' button
		And  Enter relevant data '<Name>','<Company>','<Email>','<Phone>','<PreferredContactDate>','<PreferredContactTime>','<RequestDetails>'
		
		Examples:
		| 	Name  	| 	 Company    |   					Email 					| 			Phone   | PreferredContactDate  | PreferredContactTime | RequestDetails |
    | 	"Emma"  | 		"Wiggles" |  		"Emma.wiggle@Test.com" 	| 	61455768906	|			"20/11/2017"			| 				"AM"				 | "Please send me a demo, thanks."|
		
   

  
  
   