Feature: Use the Australia Post API to calculate shipping costs for parcels of different weights to at least three countries.
  	
	@Sanity @API
  Scenario Outline: Calculate shipping costs for parcels of different weights to at least three countries
  	When I request a shipping cost providing '<CountryCode>','<ServiceCode>' and '<Weight>' 
		Then I have received HTTP response code of 200
	  And  The response body should contain expected '<Cost>'
		
		Examples:
		| 	CountryCode  	| 	 				ServiceCode 					   | 		Weight 			| 			Cost   	 |
    | 	"NZ" 				  | "INT_PARCEL_STD_OWN_PACKAGING"		 |  		1 				| 			"24.00"	 |
    | 	"US" 				  | "INT_PARCEL_COR_OWN_PACKAGING"		 |  		2 				| 			"132.7"	 |
    | 	"GB" 				  | "INT_PARCEL_EXP_OWN_PACKAGING"		 |  		3					| 			"92.10"	 |			
		
   

  
  
   