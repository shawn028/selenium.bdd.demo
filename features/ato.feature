Feature: Calculated tax using different combinations of criteria
  	
	@Sanity @ATO 
  Scenario Outline: Calculated tax using different combinations of criteria 
  	Given the user is navigate to the Simple Tax Caculator page 
		When the user select <AssessmentYear> 
		And  input<Income>
	  And  select ResidencyStatus <ResidencyStatus>
		Then Clciks on submit button 
		Then the user should see the <EstimateTaxResult>
		
		Examples:
		| AssessmentYear  | 	 Income    |    ResidencyStatus 	| EstimateTaxResult   | 
    | 	2019-20	      | 		100000	 |  		"full" 			  	| 		"$24,497.00"		|
    | 	2018-19	      | 		65000		 |  		"none"					| 		"$21,125.00"		|
    | 	2017-18	      | 		150000	 |  		"part" 					| 		"$43,656.78"		|
   

  
  
   